$(function () {
    var number = document.querySelector('.nums-counter'),
        numberTop = number.getBoundingClientRect().top,
        start = 2700, end = 3243;

    window.addEventListener('scroll', function onScroll() {
        if(window.pageYOffset > (numberTop - window.innerHeight) + 130) {
            this.removeEventListener('scroll', onScroll);
            var interval = setInterval(function() {
                number.innerHTML = ++start;
                if(start == end) {
                    clearInterval(interval);
                }
            }, 1);
        }
    });
});