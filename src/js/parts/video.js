$(function () {
    $(document).on('click', '.play-btn, .video', function () {
        $('#video-popup')
            .addClass('active')
            .find('video')[0]
            .play()
        $('.video_container video')[0].pause()
    })
    $(document).on('click', '.bg-popup, .popup_close', function (e) {
        if (!$(e.target).closest('video').length) {
            $('.bg-popup')
                .removeClass('active')
                .find('video')[0]
                .pause()
            $('.video_container video')[0].play()
        }
    })
});