$(function () {
    var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerElement: "#trigger"}});

    var scene1 = new ScrollMagic.Scene({duration: 110}).setTween(TweenMax.fromTo("#part-1", 3,  {autoAlpha:0}, {autoAlpha:1})).setPin("#part-1");
    var scene2 = new ScrollMagic.Scene({duration: 200}).setTween(TweenMax.fromTo("#part-2", 3,  {autoAlpha:0}, {autoAlpha:1})).setPin("#part-2");
    var scene3 = new ScrollMagic.Scene({duration: 185}).setTween(TweenMax.fromTo("#part-3", 3,  {autoAlpha:0}, {autoAlpha:1})).setPin("#part-3");
    var scene4 = new ScrollMagic.Scene({duration: 140}).setTween(TweenMax.fromTo("#part-4", 3,  {autoAlpha:0}, {autoAlpha:1})).setPin("#part-4");
    var scene5 = new ScrollMagic.Scene({duration: 170}).setTween(TweenMax.fromTo("#part-5", 3,  {autoAlpha:0}, {autoAlpha:1})).setPin("#part-5");
    var scene6 = new ScrollMagic.Scene({duration: 130}).setTween(TweenMax.fromTo("#part-6", 3,  {autoAlpha:0}, {autoAlpha:1})).setPin("#part-6");
    var scene7 = new ScrollMagic.Scene({duration: 210}).setTween(TweenMax.fromTo("#part-7", 3,  {autoAlpha:0}, {autoAlpha:1})).setPin("#part-7");
    var scene8 = new ScrollMagic.Scene({duration: 230}).setTween(TweenMax.fromTo("#part-8", 3,  {autoAlpha:0}, {autoAlpha:1})).setPin("#part-8");
    var scene9 = new ScrollMagic.Scene({duration: 230}).setTween(TweenMax.fromTo("#part-9", 3,  {autoAlpha:0}, {autoAlpha:1})).setPin("#part-9");
    var scene10 = new ScrollMagic.Scene({duration: 250}).setTween(TweenMax.fromTo("#part-10", 3,  {autoAlpha:0}, {autoAlpha:1})).setPin("#part-10");

    controller.addScene([scene1, scene2, scene3, scene4, scene5, scene6, scene7, scene8, scene9, scene10]);
});