$(function () {
    $slickElement = $(".components-right-slider");
    $navInfo = $(".nav-block-main-info");

    $slickElement.on('init reInit afterChange', function(event, slick, currentSlide){
        var i = (currentSlide ? currentSlide : 0) + 1;
        $navInfo.text(i + '/6');
    });

    $slickElement.on('init afterChange', function(){
        var slideTitle = $(".slick-current img").attr('alt');
        $(".nav-block-title, .components-right-name").text(slideTitle);
    });

    $slickElement.slick({
        dots: false,
        infinite: true,
        autoplay: true,
        slidesToShow: 1,
        prevArrow: "<div class='arrow-prev'></div>",
        nextArrow: "<div class='arrow-next'></div>",
        slidesToScroll: 1,
        appendArrows: ".nav-block-main",
        responsive: [
            {
                breakpoint: 1000,
                settings: {
                    centerMode: true,
                    centerPadding: '60px',
                    slidesToShow: 1,
                    arrows: false,
                    dots: false
                }
            },
            {
                breakpoint: 370,
                settings: {
                    centerMode: false,
                    slidesToShow: 1,
                    arrows: false,
                    dots: false
                }
            }
        ]
    });
});